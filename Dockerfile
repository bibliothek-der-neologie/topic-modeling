FROM python:3.6-slim-buster as base
FROM base as python-builder

RUN apt-get update; \
	apt-get install -y --no-install-recommends \
    git

COPY ./.requirements.txt /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt

FROM base

ARG USER="jupyter"
ARG GROUP="jupyter"
ARG HOME="/home/${USER}"

RUN useradd --create-home --user-group ${USER}

COPY --from=python-builder /usr/local /usr/local

COPY --chown=${USER}:${GROUP} ./.entrypoint /entrypoint


USER ${USER}

WORKDIR ${HOME}

COPY --chown=${USER}:${GROUP} . ${HOME}

EXPOSE 8888

ENTRYPOINT [ "/entrypoint" ]

CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--no-browser"]
