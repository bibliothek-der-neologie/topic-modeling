 8. Was ist Religion? – Sind es wahre, gegründete, die strengste Prüfung aushaltende,
        Gott und das Verhältniß zwischen ihm und den Menschen betreffende Sätze? – Oder sind es
        bloße Meinungen und menschliche Einfälle, Zusätze zur Religion, an welchen wir mit
        Zuversicht und Ergebenheit hängen, weil sie uns entweder von Jugend auf geläufig worden, wir
        aber das Gegentheil als wahr zu denken ungewohnt sind, oder es nur als wahr zu vermuthen und
        zu prüfen, uns nicht einmahl in den Sinn kommt; oder weil das Ansehen frommer oder in der
        Welt vielgeltender Lehrer uns für ihre Richtigkeit Gewähr zu leisten scheint; oder weil wir
        sie behaglich finden, es sei, daß sie uns eigene Untersuchung und Mühe ersparen, oder wir
        dabei keine nachtheilige, oft wohl gar gute Folgen für unsere Frömmigkeit und Gemüthsruhe
        bemerken? – Oder betreffen sie, ihrer Natur nach, Gott und das Verhältniß zwischen ihm und
        uns eigentlich, weder mittel- noch unmittelbar, gar nicht; scheinen sie uns vielmehr nur
        dahin zu gehören, weil wir sie in ehrwürdigen Büchern neben und mit Religionswahrheiten
        gefunden haben, oder unsre Einbildungskraft sie mit diesen Sätzen der Religion einmal so
        verknüpft hat, daß wir befürchten, Eins müsse mit dem Andern stehen oder fallen? – Im ersten
        Fall kann Gelehrsamkeit der Religion nicht nachtheilig seyn; sie bewährt sie eben, und hilft
        jene wahren Lehren von den erdichteten und falschen absondern. Hilft sie im zweiten Fall
        unechte Zusätze zerstören, so ist sie für die wahre Religion wohlthätig und vertilgt das
        Unkraut, unter dem wahre Religion ersticken würde. Im dritten raubt sie dem Menschen
        wenigstens nichts von Religion; aber sie macht auch den Gebrauch solcher fremden Lehren,
        wenn sie ja noch Wahrheit enthalten, für die Religion unschädlich, und zieht den Fleiß der
        Menschen von entbehrlicheren Beschäftigungen ab, und lenkt ihn auf solche, die wichtig und
        heilsam sind, hin. 