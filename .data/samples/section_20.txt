 19. Unmöglich kann die Religion ihrer Natur nach schädlich seyn. Sie wird es bloß
        durch Mißverstand, Schwärmerei und ausschweifende Leidenschaften. Dieses zu verhüten und den
        beseligenden Einfluß der Religion auf die gemeine und besondere Wohlfahrt zu befördern, sind
        in dem Staate Anstalten nöthig, wodurch immer richtigere Begriffe von Sittlichkeit und
        Religion sowohl, als wirksamster Antrieb, sie auszuüben, oder tugendhafte und gottselige
        Gesinnungen, allgemeiner gemacht werden. Weil aber die, welche fähig seyn möchten, Tugend
        und Religion auf das richtigste und nachdrücklichste zu lehren und zu empfehlen, schwerlich
        dieses Geschäft angelegentlich genug treiben werden, wenn sie sich ihm nicht ganz und
        unzerstreut widmen können; Andere hingegen, die genug Eifer haben möchten, nicht immer die
        dazu erforderlichen Fähigkeiten oder Kenntnisse besitzen, und in diesem Fall der Religion
        und dem Staate mehr schädlich als nützlich werden: so macht dies nicht nur, wie zu andern
        öffentlichen Angelegenheiten, einen besondern Stand nöthig, dergleichen man auch bei allen
        nur einigermaßen gesitteten Völkern findet; sondern der Staat hat auch die Pflicht und das
        Recht, für dessen würdigste Besetzung und für Einrichtungen zu sorgen, wodurch das
        innerliche Ansehen der dazu bestimmten Personen (§. 16. ) durch äußerliches verstärkt, und
        jede derselben in den Stand gesetzt werde, mit gehörigem Eifer und aufs wirksamste die ihr
        obliegenden Pflichten zu erfüllen. 